import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { KCom }  from './kcom';
import { TRx }  from './trx';
import { ComTelemetry } from './com.telemetry';
import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/do';  // for debugging

/**
 * This class provides the Com service with methods to read names and add names.
 */
@Injectable()
export class ComService {
  private path: String = 'https://private-036b14-guiapi.apiary-mock.com/com';

  /**
   * Creates a new NameListService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {string[]} The Observable for the HTTP request.
   */

  getKCom(): Observable<KCom> {
    return this.http.get(this.path + '/kcom')
      .map((res: Response) => res.json())
      // .do(data => console.log('server data:', data))  // debug
      .catch(this.handleError);
  }

  getTelemetry(): Observable<ComTelemetry> {
    return this.http.get(this.path + '/telemetry')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  getTrx(): Observable<TRx> {
    return this.http.get(this.path + '/trx')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   * Handle HTTP error
   */
  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

