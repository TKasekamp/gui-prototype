/**
 * This barrel file provides the export for the shared ComService.
 */
export * from './com.service';
export {KCom } from './kcom';
export {ComTelemetry} from './com.telemetry';
// export {KCom}
