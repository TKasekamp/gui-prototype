/**
 * Created by Kristjan T�nism�e on 07.03.2017.
 */
export class TRx {
  transmitMode: string;
  receiveMode: string;

  internalPA: string;
  internalLNAON: string;
}
