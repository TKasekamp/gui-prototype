/**
 * Created by Vadym Ponomarov on 25.02.2017.
 */
export class ComTelemetry {

    railCurrent: number;
    batCurrent: number;
    forwardPower: number;
    reversePower: number;
    receivedRSSI: number;
    paTemp: number;
    xoTemp: number;

    receivedPackets: number;
    droppedPackets: number;
    transmittedPackets: number;

    dcBias: number;
    tuningVoltage: number;

    icp: string;
    batSwitch: string;
    dcdcSwitch: string;
    epsSwitch: string;

    transmitMode: string;
    receiveMode: string;
    internalPA: string;
    internalLNAOn: string;
}
