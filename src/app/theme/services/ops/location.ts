export class Location {
  azimuth: string;
  elevation: string;
  latitude: string;
  longitude: string;
  range: string;
  altitude: string;
  orbit: string;
  visibility: string;
  tleepoch: string;
}
