/**
 * Created by Tõnis Kasekamp on 20.03.2017.
 */
export class GroundStation {
  id: number;
  name: string;
  status: string;
  support: string[];
  latitude: string;
  longitude: string;
  altitude: string;
  elevation: string;
  currentAzimuth: number;
  currentEl: number;
}
