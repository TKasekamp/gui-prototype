import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { GroundStation } from './gs.station';
// import 'rxjs/add/operator/do';  // for debugging

/**
 * This class provides the Ground Station  with methods to read names and add names.
 */
@Injectable()
export class GSService {
  private path: String = 'https://private-f83774-groundstation.apiary-mock.com';

  /**
   * Creates a new Ground Station Service with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {GroundStation[]} The Observable for the HTTP request.
   */

  getStations(): Observable<GroundStation[]> {
    return this.http.get(this.path + '/ground-stations')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {GroundStation} The Observable for the HTTP request.
   */

  getDetail(id: number): Observable<GroundStation> {
    return this.http.get(this.path + '/ground-station/' + id)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   * Handle HTTP error
   */
  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

