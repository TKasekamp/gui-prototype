import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { STTelemetry } from './st.telemetry';
// import 'rxjs/add/operator/do';  // for debugging

/**
 * This class provides the Star Tracker service with methods to read names and add names.
 */
@Injectable()
export class STService {
  private path: String = 'https://private-036b14-guiapi.apiary-mock.com/st';

  /**
   * Creates a new NameListService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {Telemetry} The Observable for the HTTP request.
   */

  getTelemetry(): Observable<STTelemetry> {
    return this.http.get(this.path + '/telemetry')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   * Handle HTTP error
   */
  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

