/**
 * Created by Tõnis Kasekamp on 28.02.2017.
 */
export class STTelemetry {
  timestamp: string;
  quarternion: string;
  starCount: number;
  positions: string[];
  hashes: string[];
}
