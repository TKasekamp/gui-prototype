export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'com',  // path for our page
        data: { // custom menu declaration
          menu: {
            title: 'COM', // menu title
            icon: 'ion-android-home', // menu icon
            pathMatch: 'prefix', // use it if item children not displayed in menu
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'gs',
        data: {
          menu: {
            title: 'Ground station',
            icon: 'ion-android-wifi',
            pathMatch: 'prefix',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'ops',
        data: {
          menu: {
            title: 'OPS',
            icon: 'ion-android-home',
            pathMatch: 'prefix',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'st',
        data: {
          menu: {
            title: 'Star tracker',
            icon: 'ion-star',
            pathMatch: 'prefix',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: '',
        data: {
          menu: {
            title: 'Pages',
            icon: 'ion-document',
            selected: false,
            expanded: false,
            order: 650,
          }
        },
        children: [
          {
            path: ['/login'],
            data: {
              menu: {
                title: 'Login'
              }
            }
          }
        ]
      },

      {
        path: '',
        data: {
          menu: {
            title: 'ESTCube',
            url: 'https://www.estcube.eu',
            icon: 'ion-android-exit',
            order: 800,
            target: '_blank'
          }
        }
      }
    ]
  }
];
