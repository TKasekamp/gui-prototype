import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GSComponent } from './gs.component';
import { routing } from './gs.routing';
import { NgaModule } from '../../theme/nga.module';
import { GSService } from '../../theme/services/gs/gs.service';
import { GSTableComponent } from './table/gs.table.component';


@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule
  ],
  providers: [GSService],
  declarations: [
    GSComponent,
    GSTableComponent
  ]
})
export class GSModule {
}
