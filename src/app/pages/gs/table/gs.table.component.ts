import { Component, OnInit } from '@angular/core';
import { AppState } from '../../../app.service';
import { GroundStation } from '../../../theme/services/gs/gs.station';
import { GSService } from '../../../theme/services/gs/gs.service';

@Component({
  selector: 'gs-table',
  templateUrl: 'gs.table.component.html',
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class GSTableComponent implements OnInit {

  public stations: GroundStation[];

  constructor(private _gsService: GSService, private state: AppState) {
    this.stations = [new GroundStation()]; // Dummy object
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._gsService.getStations()

      .subscribe(
        stations => {
          this.stations = stations;
          this.state.set('groundstations', stations);
        },
        error => console.log('Loadfeed', error)
      );
  }
}
