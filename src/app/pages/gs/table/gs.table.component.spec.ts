import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
// Load the implementations that should be tested
import { AppState } from '../../../app.service';
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import { GSTableComponent } from './gs.table.component';
import { GroundStation } from '../../../theme/services/gs/gs.station';
import { GSService } from '../../../theme/services/gs/gs.service';

import Spy = jasmine.Spy;

describe(`Ground station table`, () => {
  let comp: GSTableComponent;
  let fixture: ComponentFixture<GSTableComponent>;
  let gsService: GSService;
  let stations: GroundStation[];
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GSTableComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        GSService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(GSTableComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    gsService = fixture.debugElement.injector.get(GSService);

    // Dummy object setup. Should get it from json
    stations = [new GroundStation()];
    stations[0].id = 3;

    // Setup spy on the `getKCom` method
    spy = spyOn(gsService, 'getStations')
      .and.returnValue(Observable.of(stations));
  });

  it('should have a ground station list object', () => {
    expect(comp.stations).toBeDefined();
  });

  it('should not show anything before OnInit', () => {
    // All span elements
    let active = fixture.debugElement.queryAll(By.css('.table-id'));
    expect(active.length).toEqual(1, 'No table-id on page except header');
    expect(spy.calls.any()).toBe(false, 'getStations not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    fixture.detectChanges();

    // How to get an element from all html elements
    let active = fixture.debugElement.queryAll(By.css('.table-id'));
    // 0 is header
    let pos = active[1].nativeElement;

    expect(spy.calls.any()).toBe(true, 'getStations called');
    expect(comp.stations[0].id).toEqual(3, 'ID is correct');
    expect(pos.textContent).toBe('3', 'ID is correct on page');
  });

});
