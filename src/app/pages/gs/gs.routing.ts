import { Routes, RouterModule }  from '@angular/router';

import { GSComponent } from './gs.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: GSComponent,
    children: []
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
