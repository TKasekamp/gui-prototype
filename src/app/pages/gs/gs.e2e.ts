import { browser, by, element } from 'protractor';

describe('Ground station', () => {

  beforeEach(() => {
    browser.get('/#/pages/gs');
  });

  it('should have Ground Station page title', () => {
    let subject = element(by.css('h1')).getText();
    let result = 'GROUND STATION';
    expect(subject).toEqual(result);
  });

  it('should have <gs-table>', () => {
    let subject = element(by.css('gs-table')).isPresent();
    let result = true;
    expect(subject).toEqual(result);
  });

  it('should have 2 items in <gs-table>', () => {
    let subject = element.all(by.css('.no-top-border'));
    let result = 2;
    expect(subject.count()).toEqual(result);
  });

  it('should have GS table widget title', () => {
    let subject = element(by.css('.card-title')).getText();
    let result = 'REGISTERED GROUND STATIONS';
    expect(subject).toEqual(result);
  });

});
