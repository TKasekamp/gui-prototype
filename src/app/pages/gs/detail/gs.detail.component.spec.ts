import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
// Load the implementations that should be tested
import { AppState } from '../../../app.service';
import { GSDetailComponent } from './gs.detail.component';
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import { GSService } from '../../../theme/services/gs/gs.service';
import { GroundStation } from '../../../theme/services/gs/gs.station';
import Spy = jasmine.Spy;

describe(`Ground station detail`, () => {
  let comp: GSDetailComponent;
  let fixture: ComponentFixture<GSDetailComponent>;
  let gsService: GSService;
  let detail: GroundStation;
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GSDetailComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        GSService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(GSDetailComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    gsService = fixture.debugElement.injector.get(GSService);

    // Dummy object setup. Should get it from json
    detail = new GroundStation();
    detail.status = 'OK';

    // Setup spy on the `getTrx` method
    spy = spyOn(gsService, 'getDetail')
      .and.returnValue(Observable.of(detail));
  });

  it('should have a detail object', () => {
    expect(comp.detail).toBeDefined();
  });

  it('should not show anything before OnInit', () => {
    let active = fixture.debugElement.queryAll(By.css('.status-button'));
    expect(active[0].nativeElement.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getDetail not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    // Before is nothing
    let active = fixture.debugElement.queryAll(By.css('.status-button'));
    expect(active[0].nativeElement.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getDetail not yet called');

    fixture.detectChanges();
    // comp.ngOnInit();

    // After
    expect(spy.calls.any()).toBe(true, 'getDetail called');
    expect(comp.detail.status).toBe('OK', 'OK status');
    expect(active[0].nativeElement.textContent).toBe('OK', 'OK content');
  });

});
