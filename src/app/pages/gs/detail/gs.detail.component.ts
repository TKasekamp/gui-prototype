import { Component, OnInit, Input } from '@angular/core';
import { AppState } from '../../../app.service';
import { GroundStationDetail } from '../../../theme/services/gs/gs.detail';
import { GSService } from '../../../theme/services/gs/gs.service';
import { GroundStation } from '../../../theme/services/gs/gs.station';

@Component({
  selector: 'gs-detail',
  templateUrl: 'gs.detail.component.html'
})

export class GSDetailComponent implements OnInit {
  @Input() id: number;

  public detail: GroundStation;

  constructor(private _gsService: GSService, private state: AppState) {
    this.detail = new GroundStation();
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._gsService.getDetail(this.id)

      .subscribe(
        detail => {
          this.detail = detail;
          this.state.set('gs-detail', detail);
        },
        error => console.log('Loadfeed', error)
      );
  }
}
