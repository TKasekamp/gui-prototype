import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';
import { Component } from '@angular/core';
import {
  BaseRequestOptions,
  ConnectionBackend,
  Http
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

// Load the implementations that should be tested
import { AppState } from '../../../app.service';
import { TrxComponent } from './trx.component';
import { ComService } from '../../../theme/services/com/com.service';
import { TRx } from '../../../theme/services/com/trx';
import Spy = jasmine.Spy;
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';

describe(`Trx`, () => {
  let comp: TrxComponent;
  let fixture: ComponentFixture<TrxComponent>;
  let comService: ComService;
  let tRx: TRx;
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrxComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        ComService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(TrxComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    comService = fixture.debugElement.injector.get(ComService);

    // Dummy object setup. Should get it from json
    tRx = new TRx();
    tRx.transmitMode = 'OK';

    // Setup spy on the `getTrx` method
    spy = spyOn(comService, 'getTrx')
      .and.returnValue(Observable.of(tRx));
  });

  it('should have a trx object', () => {
    expect(comp.trx).toBeDefined();

    // expect(comp.trx.transmitMode).toBe("");
  });

  it('should not show anything before OnInit', () => {
    let active = fixture.debugElement.query(By.css('.status-button')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getTrx not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    // Before is nothing
    let active = fixture.debugElement.query(By.css('.status-button')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getTrx not yet called');

    fixture.detectChanges();
    // comp.ngOnInit();
    // After
    expect(spy.calls.any()).toBe(true, 'getTrx called');
    expect(comp.trx.transmitMode).toBe('OK', 'OK trx');
    expect(active.textContent).toBe('OK', 'OK content');
  });

});
