import { Component, OnInit } from '@angular/core';
import { TRx } from '../../../theme/services/com/trx';
import { ComService } from '../../../theme/services/com/com.service';
import { AppState } from '../../../app.service';

@Component({
  selector: 'com-trx',
  templateUrl: './trx.component.html'
})

export class TrxComponent implements OnInit {

  public trx: TRx;

  constructor(private _comService: ComService, private state: AppState) {
    this.trx = new TRx();
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._comService.getTrx()

      .subscribe(
        trx => {
          this.trx = trx;
          this.state.set('trx', trx);
        },
        error => console.log('Loadfeed', error)
      );
  }
}
