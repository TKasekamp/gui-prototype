import { browser, by, element } from 'protractor';

describe('Com', () => {

  beforeEach(() => {
    browser.get('/#/pages/com');
  });

  it('should have <com-kcom>', () => {
    let subject = element(by.css('com-kcom')).isPresent();
    let result = true;
    expect(subject).toEqual(result);
  });

  it('should have <com-telemetry>', () => {
    let subject = element(by.css('com-telemetry')).isPresent();
    let result = true;
    expect(subject).toEqual(result);
  });

  it('should have <com-trx>', () => {
    let subject = element(by.css('com-trx')).isPresent();
    let result = true;
    expect(subject).toEqual(result);
  });

  it('should have Com page title', () => {
    let subject = element(by.css('h1')).getText();
    let result = 'COM';
    expect(subject).toEqual(result);
  });


  it('should have ComTelemetry widget title', () => {
    let subject = element.all(by.css('.card-title')).get(0).getText();
    let result = 'TELEMETRY';
    expect(subject).toEqual(result);
  });

  it('should have KCom widget title', () => {
    let subject = element.all(by.css('.card-title')).get(1).getText();
    let result = 'KCOM';
    expect(subject).toEqual(result);
  });

  it('should have Trx widget title', () => {
    let subject = element.all(by.css('.card-title')).get(2).getText();
    let result = 'TRX';
    expect(subject).toEqual(result);
  });


});
