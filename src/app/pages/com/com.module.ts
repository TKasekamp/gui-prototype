import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { ComComponent } from './com.component';
import { routing } from './com.routing';
import { NgaModule } from '../../theme/nga.module';
import { ComService } from '../../theme/services/com/com.service';
import { KComComponent } from './kcom/kcom.component';
import { TelemetryComponent } from './telemetry/telemetry.component';
import { TrxComponent } from './trx/trx.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule
  ],
  providers: [ComService],
  declarations: [
    ComComponent,
    KComComponent,
    TelemetryComponent,
    TrxComponent
  ]
})
export class ComModule {
}
