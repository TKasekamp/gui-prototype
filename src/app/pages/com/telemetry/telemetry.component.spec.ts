import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';
import { Component } from '@angular/core';
import {
  BaseRequestOptions,
  ConnectionBackend,
  Http
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

// Load the implementations that should be tested
import { AppState } from '../../../app.service';
import { TelemetryComponent } from './telemetry.component';
import { ComService } from '../../../theme/services/com/com.service';
import { ComTelemetry } from '../../../theme/services/com/com.telemetry';
import Spy = jasmine.Spy;
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';

describe(`Com telemetry`, () => {
  let comp: TelemetryComponent;
  let fixture: ComponentFixture<TelemetryComponent>;
  let comService: ComService;
  let telemetry: ComTelemetry;
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TelemetryComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        ComService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(TelemetryComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    comService = fixture.debugElement.injector.get(ComService);

    // Dummy object setup. Should get it from json
    telemetry = new ComTelemetry();
    telemetry.railCurrent = 4;

    // Setup spy on the `getKCom` method
    spy = spyOn(comService, 'getTelemetry')
      .and.returnValue(Observable.of(telemetry));
  });

  it('should have a telemetry object', () => {
    expect(comp.telemetry).toBeDefined();

    // expect(comp.kcom.active).toBe("");
  });

  it('should not show anything before OnInit', () => {
    let active = fixture.debugElement.query(By.css('#railCurrentField')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getTelemetry not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    // Before is nothing
    let active = fixture.debugElement.query(By.css('#railCurrentField')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getTelemetry not yet called');

    fixture.detectChanges();
    // comp.ngOnInit();
    // After
    expect(spy.calls.any()).toBe(true, 'getTelemetry called');
    expect(comp.telemetry.railCurrent).toBe(4, 'Railcurrent 4');
    expect(active.textContent).toBe('4', 'Railcurrent 4');
  });

});
