import { Component, OnInit } from '@angular/core';
import { ComTelemetry } from '../../../theme/services/com/com.telemetry';
import { ComService } from '../../../theme/services/com/com.service';
import { AppState } from '../../../app.service';

@Component({
  selector: 'com-telemetry',
  templateUrl: './telemetry.component.html'

})

export class TelemetryComponent implements OnInit {

  public telemetry: ComTelemetry;

  constructor(private _comService: ComService, private state: AppState) {
    this.telemetry = new ComTelemetry();
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._comService.getTelemetry()

      .subscribe(
        telemetry => {
          this.telemetry = telemetry;
          this.state.set('comtelemetry', telemetry);
        },
        error => console.log('Loadfeed', error)
      );
  }
}
