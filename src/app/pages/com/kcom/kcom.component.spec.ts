import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';
import { Component } from '@angular/core';
import {
  BaseRequestOptions,
  ConnectionBackend,
  Http
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

// Load the implementations that should be tested
import { AppState } from '../../../app.service';
import { KComComponent } from './kcom.component';
import { ComService } from '../../../theme/services/com/com.service';
import { KCom } from '../../../theme/services/com/kcom';
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import Spy = jasmine.Spy;


describe(`KCom`, () => {
  let comp: KComComponent;
  let fixture: ComponentFixture<KComComponent>;
  let comService: ComService;
  let kCom: KCom;
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KComComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        ComService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(KComComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    comService = fixture.debugElement.injector.get(ComService);

    // Dummy object setup. Should get it from json
    kCom = new KCom();
    kCom.active = 'OK';

    // Setup spy on the `getKCom` method
    spy = spyOn(comService, 'getKCom')
      .and.returnValue(Observable.of(kCom));
  });

  it('should have a kcom object', () => {
    expect(comp.kcom).toBeDefined();

    // expect(comp.kcom.active).toBe("");
  });

  it('should not show anything before OnInit', () => {
    let active = fixture.debugElement.query(By.css('.status-button')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getKCom not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    // Before is nothing
    let active = fixture.debugElement.query(By.css('.status-button')).nativeElement;
    expect(active.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getKCom not yet called');

    fixture.detectChanges();
    // comp.ngOnInit();
    // After
    expect(spy.calls.any()).toBe(true, 'getKCom called');
    expect(comp.kcom.active).toBe('OK', 'OK kcom');
    expect(active.textContent).toBe('OK', 'OK content');
  });

});
