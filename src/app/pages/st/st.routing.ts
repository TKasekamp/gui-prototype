import { Routes, RouterModule }  from '@angular/router';

import { STComponent } from './st.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: STComponent,
    children: []
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
