import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';
import { Component } from '@angular/core';
import {
  BaseRequestOptions,
  ConnectionBackend,
  Http
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

// Load the implementations that should be tested
import { AppState } from '../../../app.service';

import Spy = jasmine.Spy;
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import { STService } from '../../../theme/services/st/st.service';
import { STTelemetry } from '../../../theme/services/st/st.telemetry';
import { TelemetryComponent } from './telemetry.component';

describe(`Star tracker telemetry`, () => {
  let comp: TelemetryComponent;
  let fixture: ComponentFixture<TelemetryComponent>;
  let stService: STService;
  let telemetry: STTelemetry;
  let spy: Spy;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TelemetryComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BaseRequestOptions,
        MockBackend,
        STService,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        AppState,

      ]
    })
      .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(TelemetryComponent);
    comp = fixture.componentInstance;

    // fixture.detectChanges(); // trigger initial data binding

    // ComService actually injected into the component
    stService = fixture.debugElement.injector.get(STService);

    // Dummy object setup. Should get it from json
    telemetry = new STTelemetry();
    telemetry.positions = ['thing'];

    // Setup spy on the `getKCom` method
    spy = spyOn(stService, 'getTelemetry')
      .and.returnValue(Observable.of(telemetry));
  });

  it('should have a telemetry object', () => {
    expect(comp.telemetry).toBeDefined();
  });

  it('should not show anything before OnInit', () => {
    // All span elements
    let active = fixture.debugElement.queryAll(By.css('span'));
    expect(active.length).toEqual(0, 'No spans on page');
    expect(spy.calls.any()).toBe(false, 'getTelemetry not yet called');
  });

  it('should loadfeed ngOnInit', () => {
    fixture.detectChanges();

    // How to get an element from all html elements
    let active = fixture.debugElement.queryAll(By.css('span'));
    let pos = active[0].nativeElement;

    expect(spy.calls.any()).toBe(true, 'getTelemetry called');
    expect(comp.telemetry.positions[0]).toEqual('thing', 'Correct number of stars');
    expect(pos.textContent).toBe('thing', 'First position present');
  });

});
