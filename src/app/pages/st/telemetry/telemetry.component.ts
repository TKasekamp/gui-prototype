import { Component, OnInit } from '@angular/core';
import { AppState } from '../../../app.service';
import { STTelemetry } from '../../../theme/services/st/st.telemetry';
import { STService } from '../../../theme/services/st/st.service';

@Component({
  selector: 'st-telemetry',
  templateUrl: 'telemetry.component.html',
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TelemetryComponent implements OnInit {

  public telemetry: STTelemetry;

  constructor(private _stService: STService, private state: AppState) {
    this.telemetry = new STTelemetry(); // Dummy object
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._stService.getTelemetry()

      .subscribe(
        telemetry => {
          this.telemetry = telemetry;
          this.state.set('stTelemetry', telemetry);
        },
        error => console.log('Load feed', error)
      );
  }
}
