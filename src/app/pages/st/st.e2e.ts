import { browser, by, element } from 'protractor';

describe('Star tracker', () => {

  beforeEach(() => {
    browser.get('/#/pages/st');
  });

  it('should have Star tracker page title', () => {
    let subject = element(by.css('h1')).getText();
    let result = 'STAR TRACKER';
    expect(subject).toEqual(result);
  });

  it('should have <st-telemetry>', () => {
    let subject = element(by.css('st-telemetry')).isPresent();
    let result = true;
    expect(subject).toEqual(result);
  });

  it('should have Telemetry widget title', () => {
    let subject = element(by.css('.card-title')).getText();
    let result = 'TELEMETRY';
    expect(subject).toEqual(result);
  });

});
