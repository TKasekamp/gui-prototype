import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { STComponent } from './st.component';
import { routing } from './st.routing';
import { NgaModule } from '../../theme/nga.module';
import { STService } from '../../theme/services/st/st.service';
import { TelemetryComponent } from './telemetry/telemetry.component';


@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule
  ],
  providers: [STService],
  declarations: [
    STComponent,
    TelemetryComponent
  ]
})
export class STModule {
}
