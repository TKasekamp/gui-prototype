import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule' },
      { path: 'com', loadChildren: 'app/pages/com/com.module#ComModule' },
      { path: 'gs', loadChildren: 'app/pages/gs/gs.module#GSModule' },
      { path: 'ops', loadChildren: 'app/pages/ops/ops.module#OpsModule' },
      { path: 'st', loadChildren: 'app/pages/st/st.module#STModule' }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
