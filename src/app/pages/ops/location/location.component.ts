import { Component } from '@angular/core';
import { Location } from '../../../theme/services/ops/location';
import { OpsService } from '../../../theme/services/ops/ops.service';
import { AppState } from '../../../app.service';

@Component({
  selector: 'ops-location',
  templateUrl: 'location.component.html'
})

export class LocationComponent {

  public location: Location;

  constructor(private _opsService: OpsService, private state: AppState) {
    this.location = new Location();
  }

  ngOnInit() {
    this._loadFeed();
  }

  private _loadFeed() {
    this._opsService.getLocation()

      .subscribe(
        location => {
          this.location = location;
        },
        error => console.log('Load feed', error)
      );
  }
}
