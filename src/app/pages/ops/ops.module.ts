import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { OpsComponent } from './ops.component';
import { routing } from './ops.routing';
import { NgaModule } from '../../theme/nga.module';
import { OpsService } from '../../theme/services/ops/ops.service';
import { LocationComponent } from './location/location.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule
  ],
  providers: [OpsService],
  declarations: [
    OpsComponent,
    LocationComponent
  ]
})
export class OpsModule {
}
