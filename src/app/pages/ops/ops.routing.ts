import { Routes, RouterModule }  from '@angular/router';
import { OpsComponent } from './ops.component';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: OpsComponent,
    children: []
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
