# ESTCube-2 Mission Control System Graphical User Interface
[![build status](https://gitlab.com/TKasekamp/gui-prototype/badges/master/build.svg)](https://gitlab.com/TKasekamp/gui-prototype/commits/master)
[![coverage report](https://gitlab.com/TKasekamp/gui-prototype/badges/master/coverage.svg)](https://tkasekamp.gitlab.io/gui-prototype/)


dasdasd

The code is based on [ng2-admin](https://akveo.github.io/ng2-admin/) from Akveo. Follow their guides for code examples.

Staging server on [Heroku](https://estcube2-staging.herokuapp.com/).
## Technology
* Typescript
* Angular 2
* SASS
* Bootstrap 4
* Webpack
* Karma
* Protractor

## How to run
Download npm from somewhere. Then do `npm install` and `npm run server`. Go to localhost:3000 and you should see the page!.

`npm run test` to run lint and Karma tests.

`npm run protractor` to run Protractor tests. Note that you must have the server running with `npm run server`.
## Development tools
Whatever you want, but probably use Jetbrains WebStorm. 

## Code style
Use the code style provided in `tslint.json`.

## Documentation
From [MCS confluence](https://confluence.tudengisatelliit.ut.ee:8433/display/EC2MCS/ESTCube+2+-+Mission+Control+System) find the GUI page.
